'use strict';

const https = require('https');
const http = require('http');
const fs = require('fs');
const url = 'https://translate.yandex.net/api/v1.5/tr.json/translate';
const key = 'trnsl.1.1.20160723T183155Z.f2a3339517e26a3c.d86d2dc91f2e374351379bb3fe371985273278df';
const port = 3000;
const server = http.createServer();

const handler = (req, res) => {
	if (req.method.toLowerCase() == 'get') {
		displayForm(res);
	} else if (req.method.toLowerCase() == 'post') {
		requestTranslation(req, res);
	}
};

const displayForm = res => {
	fs.readFile('form.html', (err, data) => {
		res.writeHead(200, {
			'Content-Type': 'text/html',
			'Content-Length': data.length
		});

		res.write(data);
		res.end();
	});
};

const requestTranslation = (req, res) => {
	req.on('data', chunk => {
		https
			.get(`${url}?key=${key}&${chunk.toString()}`)
			.on('error', err => console.error(err))
			.on('response', response => {
				response.on('data', chunk => {
					res.writeHead(200, {
						'Content-Type': 'text/plain; charset=utf-8'
					});

					res.write(`Перевод: ${JSON.parse(chunk).text}`);
					res.end();
				});
			});
	});
};

server.on('error', err => console.error(err));
server.on('request', handler);
server.on('listening', () => {
	console.log('Start HTTP on port %d', port);
})

server.listen(port);